0.9
------
- added sri() helper

0.8
------
- added default parameter to hashed_asset() helper

0.7
------
- added validation rule "alpha_space"

0.6
------
- bugfix for hashed_asset() helper with non-existing files
- updated composer tags

0.5
------
- added hashed_asset() helper function

0.4
------
- allow route() as active helper parameter

0.3
------
- added functions

0.2
------
- switched to global functions instead facades

0.1
------
- initial commit