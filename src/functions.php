<?php

if (!function_exists('helper')) {
    function helper()
    {
        return app('helper');
    }
}

if (!function_exists('error')) {
    function error($errors, $name)
    {
        if ($errors->has($name)) {
            return '<div class="form-control-feedback">' . $errors->first($name) . '</div>';
        }
    }
}

if (!function_exists('active')) {
    function active($routes, $onlyClass = true)
    {
        if (!is_array($routes)) {
            $routes = [$routes];
        }
        foreach ($routes as $route) {
            if (request()->is($route) || (\Route::has($route) && route($route) == request()->url())) {
                if ($onlyClass) {
                    echo ' active';
                } else {
                    echo ' class="active"';
                }
                break;
            }
        }
    }
}

if (!function_exists('hashed_asset')) {
    function hashed_asset($url, $default = null, $file = null)
    {
        $path = is_null($file) ? public_path($url) : $file;

        if (file_exists($path)) {
            $hash = filemtime($path);

            return asset($url . (strpos($url, '?') === false ? '?' : '&') . 'hash=' . $hash);
        }

        return $default ?? '';
    }
}

if (!function_exists('sri')) {
    function sri($file)
    {
        $cacheName = $file.'-sri@'.filemtime($file);
        $sri = cache($cacheName);

        if (is_null($sri)) {
            $hash = hash('sha256', file_get_contents($file), true);
            $hash_base64 = base64_encode($hash);
            $sri = "sha256-$hash_base64";

            cache([$cacheName => $sri], 60 * 60 * 24 *30);
        }

        return $sri;
    }
}