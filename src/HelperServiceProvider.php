<?php
namespace Enso\Helper;

use Illuminate\Support\ServiceProvider;
use Enso\Helper\Controllers\HelperController;
use Validator;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('alpha_space', function($attribute, $value) {
            return preg_match('/^[\pL\s]+$/u', $value);
        }, 'The :attribute may only contain letters and spaces.');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('helper', function() {
            return new HelperController();
        });
    }
}
